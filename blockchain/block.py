#!/usr/bin/env python

import time
import hashlib

class Block:
    def __init__(self, previous_hash, transaction_list):
        self.previous_hash = previous_hash
        self.transaction_list = transaction_list
        self.timestamp = time.time()
        self.current_hash = self.compute_hash()
    
    def compute_hash(self):
        data = str(self.previous_hash).join(self.transaction_list) + str(self.timestamp)
        return hashlib.sha256(data.encode('utf-8'))

    def __str__(self):
        return "{previous_hash: " + str(self.previous_hash.hexdigest()) + ", current_hash: " + str(self.current_hash.hexdigest()) + ", timestamp: " + str(self.timestamp) + "}"