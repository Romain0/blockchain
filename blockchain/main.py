#!/usr/bin/env python

from blockchain import Blockchain

blockchain = Blockchain()
blockchain.create_genesis_block()

for x in range(0, 5):
    blockchain.generate_block()