#!/usr/bin/env python

from block import Block
import hashlib

class Blockchain:
    def __init__(self):
        self.block_list = []
        self.first_hash = hashlib.sha256(b"Initial hash of blockchain")
        self.default_hash = hashlib.sha256(b"This is a hash !")
        self.timestamp = 1633956286.00

    def create_genesis_block(self):
        genesis_block = Block(self.first_hash, [])
        genesis_block.timestamp = self.timestamp
        genesis_block.current_hash = self.default_hash
        self.block_list.append(genesis_block)

    def get_latest_block(self):
        return self.block_list[-1]

    def is_valid_block(self, prev_block, next_block):
        return prev_block.current_hash == next_block.previous_hash

    def generate_block(self):
        last_element_of_list = self.get_latest_block()
        block = Block(last_element_of_list.current_hash, [])
        if self.is_valid_block(last_element_of_list, block):
            self.block_list.append(block)
        else:
            print("Invalid block")

        if len(self.block_list) % 5 == 0:
            self.is_valid_chain()

    def is_valid_chain(self):
        is_valid = True

        for i in range(len(self.block_list), 0, -1):
        # for block in reversed(self.block_list):
            if i == 1:
                if self.is_valid_genesis_block(self.block_list[i-1]) == False:
                    is_valid = False
                    break
            else:
                if self.block_list[i-2].current_hash != self.block_list[i-1].previous_hash:
                    is_valid = False
                    break
            
        if is_valid == False:
            print("La chaine n'est pas valide ! Dommage c'est perdu")
            print("Try again !")
            quit()
        else:
            print("La chaine est valide.")
        

    def is_valid_genesis_block(self, genesis_block):
        block = Block(self.first_hash, [])
        block.timestamp = self.timestamp
        block.current_hash = self.default_hash

        if genesis_block.timestamp == block.timestamp and genesis_block.current_hash == block.current_hash and genesis_block.transaction_list == block.transaction_list:
            return True

        return False
