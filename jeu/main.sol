pragma solidity >=0.7.0 <0.9.0;

contract Main {
    int256 public userScore = 0;
    int256 public computerScore = 0;
    int256 public round = 1;
    string public text = "";

    // A matrix containing result of the game depending on its states
    mapping(string => mapping(string => int256)) private states;

    // The constructor initialise the game environment
    constructor() {
        states["R"]["R"] = 0;
        states["R"]["P"] = 2;
        states["R"]["S"] = 1;
        states["P"]["R"] = 1;
        states["P"]["P"] = 0;
        states["P"]["S"] = 2;
        states["S"]["R"] = 2;
        states["S"]["P"] = 1;
        states["S"]["S"] = 0;
    }

    function play() public {
        string memory userChoice = random(1);
        string memory computerChoice = random(2);

        result(userChoice, computerChoice);
        round++;
    }

    function pierre() public {
        string memory userChoice = "R";
        string memory computerChoice = random(2);

        result(userChoice, computerChoice);
        round++;
    }

    function papier() public {
        string memory userChoice = "P";
        string memory computerChoice = random(2);

        result(userChoice, computerChoice);
        round++;
    }

    function ciseau() public {
        string memory userChoice = "S";
        string memory computerChoice = random(2);

        result(userChoice, computerChoice);
        round++;
    }

    function random(uint256 i) internal view returns (string memory) {
        uint256 randomnumber = uint256(
            keccak256(abi.encodePacked(block.timestamp + i, block.difficulty))
        ) % 3;
        randomnumber = randomnumber + 1;
        if (int256(randomnumber) == int256(1)) {
            return "R";
        } else if (int256(randomnumber) == int256(2)) {
            return "P";
        } else {
            return "S";
        }
    }

    function result(string memory userChoice, string memory computerChoice)
        internal
    {
        // Papier < Ciseaux
        // Papier > Pierre
        // Ciseau < Pierre
        int256 roundResult = states[userChoice][computerChoice];

        if (int256(roundResult) == int256(1)) {
            userScore++;
            text = "Victoire joueur";
        } else if (int256(roundResult) == int256(2)) {
            computerScore++;
            text = "Victoire machine";
        } else {
            text = "Draw";
        }
    }
}
